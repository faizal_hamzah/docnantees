<?php
class DATABASE_CONFIG {

	public $default = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'root',
		'password' => '',
		'database' => 'nucleus_docnantees',
		'unix_socket' => '/Applications/XAMPP/xamppfiles/var/mysql/mysql.sock',
	);
}
