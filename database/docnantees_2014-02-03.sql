# ************************************************************
# Sequel Pro SQL dump
# Version 3408
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.1.44-log)
# Database: nucleus_docnantees
# Generation Time: 2014-02-03 05:12:50 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table acos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acos`;

CREATE TABLE `acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `acos` WRITE;
/*!40000 ALTER TABLE `acos` DISABLE KEYS */;

INSERT INTO `acos` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`)
VALUES
	(1,NULL,NULL,NULL,'controllers',1,160),
	(9,1,NULL,NULL,'AclManagement',2,37),
	(10,9,NULL,NULL,'Groups',3,14),
	(11,10,NULL,NULL,'index',4,5),
	(12,10,NULL,NULL,'view',6,7),
	(13,10,NULL,NULL,'add',8,9),
	(14,10,NULL,NULL,'edit',10,11),
	(15,10,NULL,NULL,'delete',12,13),
	(16,9,NULL,NULL,'UserPermissions',15,24),
	(17,16,NULL,NULL,'index',16,17),
	(18,16,NULL,NULL,'sync',18,19),
	(19,16,NULL,NULL,'edit',20,21),
	(20,16,NULL,NULL,'toggle',22,23),
	(21,9,NULL,NULL,'Users',25,36),
	(24,21,NULL,NULL,'index',26,27),
	(25,21,NULL,NULL,'view',28,29),
	(26,21,NULL,NULL,'add',30,31),
	(27,21,NULL,NULL,'edit',32,33),
	(28,21,NULL,NULL,'delete',34,35),
	(30,1,NULL,NULL,'Banners',38,49),
	(31,30,NULL,NULL,'index',39,40),
	(32,30,NULL,NULL,'view',41,42),
	(33,30,NULL,NULL,'add',43,44),
	(34,30,NULL,NULL,'edit',45,46),
	(35,30,NULL,NULL,'delete',47,48),
	(36,1,NULL,NULL,'Categories',50,63),
	(37,36,NULL,NULL,'index',51,52),
	(38,36,NULL,NULL,'view',53,54),
	(39,36,NULL,NULL,'add',55,56),
	(40,36,NULL,NULL,'edit',57,58),
	(41,36,NULL,NULL,'delete',59,60),
	(42,1,NULL,NULL,'Designs',64,77),
	(43,42,NULL,NULL,'index',65,66),
	(44,42,NULL,NULL,'view',67,68),
	(45,42,NULL,NULL,'add',69,70),
	(46,42,NULL,NULL,'edit',71,72),
	(47,42,NULL,NULL,'delete',73,74),
	(48,1,NULL,NULL,'Pages',78,81),
	(49,48,NULL,NULL,'display',79,80),
	(50,1,NULL,NULL,'Tags',82,93),
	(51,50,NULL,NULL,'index',83,84),
	(52,50,NULL,NULL,'view',85,86),
	(53,50,NULL,NULL,'add',87,88),
	(54,50,NULL,NULL,'edit',89,90),
	(55,50,NULL,NULL,'delete',91,92),
	(56,1,NULL,NULL,'Themes',94,105),
	(57,56,NULL,NULL,'index',95,96),
	(58,56,NULL,NULL,'view',97,98),
	(59,56,NULL,NULL,'add',99,100),
	(60,56,NULL,NULL,'edit',101,102),
	(61,56,NULL,NULL,'delete',103,104),
	(62,1,NULL,NULL,'Users',106,117),
	(63,62,NULL,NULL,'index',107,108),
	(64,62,NULL,NULL,'view',109,110),
	(65,62,NULL,NULL,'add',111,112),
	(66,62,NULL,NULL,'edit',113,114),
	(67,62,NULL,NULL,'delete',115,116),
	(68,1,NULL,NULL,'Votes',118,129),
	(69,68,NULL,NULL,'index',119,120),
	(70,68,NULL,NULL,'view',121,122),
	(71,68,NULL,NULL,'add',123,124),
	(72,68,NULL,NULL,'edit',125,126),
	(73,68,NULL,NULL,'delete',127,128),
	(74,1,NULL,NULL,'Challenges',130,143),
	(75,74,NULL,NULL,'index',131,132),
	(76,74,NULL,NULL,'view',133,134),
	(77,74,NULL,NULL,'add',135,136),
	(78,74,NULL,NULL,'edit',137,138),
	(79,74,NULL,NULL,'delete',139,140),
	(80,1,NULL,NULL,'Uploader',144,145),
	(81,1,NULL,NULL,'Prizes',146,157),
	(82,81,NULL,NULL,'index',147,148),
	(83,81,NULL,NULL,'view',149,150),
	(84,81,NULL,NULL,'add',151,152),
	(85,81,NULL,NULL,'edit',153,154),
	(86,81,NULL,NULL,'delete',155,156),
	(88,36,NULL,NULL,'getList',61,62),
	(89,42,NULL,NULL,'challenge',75,76),
	(90,74,NULL,NULL,'getList',141,142),
	(91,1,NULL,NULL,'Facebook',158,159);

/*!40000 ALTER TABLE `acos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table aros
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aros`;

CREATE TABLE `aros` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `aros` WRITE;
/*!40000 ALTER TABLE `aros` DISABLE KEYS */;

INSERT INTO `aros` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`)
VALUES
	(1,NULL,'Group',1,NULL,1,4),
	(2,NULL,'Group',2,NULL,5,10),
	(3,1,'User',1,NULL,2,3),
	(4,2,'User',2,NULL,6,7),
	(5,2,'User',3,NULL,8,9);

/*!40000 ALTER TABLE `aros` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table aros_acos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aros_acos`;

CREATE TABLE `aros_acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) NOT NULL,
  `aco_id` int(10) NOT NULL,
  `_create` varchar(2) NOT NULL DEFAULT '0',
  `_read` varchar(2) NOT NULL DEFAULT '0',
  `_update` varchar(2) NOT NULL DEFAULT '0',
  `_delete` varchar(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ARO_ACO_KEY` (`aro_id`,`aco_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `aros_acos` WRITE;
/*!40000 ALTER TABLE `aros_acos` DISABLE KEYS */;

INSERT INTO `aros_acos` (`id`, `aro_id`, `aco_id`, `_create`, `_read`, `_update`, `_delete`)
VALUES
	(1,1,1,'1','1','1','1');

/*!40000 ALTER TABLE `aros_acos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table banners
# ------------------------------------------------------------

DROP TABLE IF EXISTS `banners`;

CREATE TABLE `banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `image_url` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `modified_by` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `shortcode` varchar(200) DEFAULT NULL,
  `description` varchar(100) DEFAULT '',
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `rght` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;

INSERT INTO `categories` (`id`, `name`, `shortcode`, `description`, `parent_id`, `lft`, `rght`)
VALUES
	(1,'My Categories','categories','',NULL,1,30),
	(2,'Fun',NULL,'',1,2,15),
	(3,'Sport',NULL,'',2,3,8),
	(4,'Surfing',NULL,'',3,4,5),
	(5,'Extreme knitting',NULL,'',3,6,7),
	(6,'Friends',NULL,'',2,9,14),
	(7,'Gerald',NULL,'',6,10,11),
	(8,'Gwendolyn',NULL,'',6,12,13),
	(9,'Work',NULL,'',1,16,29),
	(10,'Reports',NULL,'',9,17,22),
	(11,'Annual',NULL,'',10,18,19),
	(12,'Status',NULL,'',10,20,21),
	(13,'Trips',NULL,'',9,23,28),
	(14,'National',NULL,'',13,24,25),
	(15,'International',NULL,'',13,26,27);

/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table challenges
# ------------------------------------------------------------

DROP TABLE IF EXISTS `challenges`;

CREATE TABLE `challenges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL DEFAULT '',
  `descriptions` text,
  `image` varchar(200) DEFAULT NULL,
  `header` varchar(250) DEFAULT NULL,
  `startDate` datetime DEFAULT NULL,
  `endDate` datetime DEFAULT NULL,
  `shortcode` varchar(200) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `challenges` WRITE;
/*!40000 ALTER TABLE `challenges` DISABLE KEYS */;

INSERT INTO `challenges` (`id`, `name`, `descriptions`, `image`, `header`, `startDate`, `endDate`, `shortcode`, `status`)
VALUES
	(1,'Summer','Whether your part of the world is basking in sunshine or suffering in blustery cold, put on your real or imaginary tank top and think summer.\r\n\r\nConsider all the iconic imagery, colors and feelings that represent the time of year when sleeves are torn off and jeans are cut short. Bring your favorite parts of the warmest season together and submit a design for a tank or a tee.\r\n\r\nDesign whatever summer means to you but keep your designs simple. Try to limit yourself to two or three colors because in that time of year, less is more.\r\n\r\nSurfs up!','/files/uploads/summer_mainbanner.jpg','Design a tank or tee inspired by summer','2014-01-01 12:49:00','2014-01-31 12:49:00','summer','ongoing'),
	(2,'Draw Your Wish','The holiday season seems to bring out the dreamer in all of us.\r\n\r\nWe find ourselves drifting off at our desks, thinking about all the awesome things that could be. Maybe it\'s the promise of a new year, or the idea that your dreams can come true in this season, that makes us really believe our wishes could come true.\r\n\r\nYour challenge is to create a design for a tee inspired by one of your wishes.\r\n\r\nDo you wish for unlimited video games? Teleportation? Cats and dogs, living together in harmony? Whatever your dream is, put it on a tee! Maybe it will help it come true! (It\'d be kind of like Oprah\'s Vision board, but wearable.)\r\n\r\nClose your eyes, make a wish, and draw it up!','/files/uploads/drawyourwish_mainbanner.jpg','',NULL,NULL,'drawyourwish','completed');

/*!40000 ALTER TABLE `challenges` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table designs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `designs`;

CREATE TABLE `designs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(200) NOT NULL,
  `challenge_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `story` varchar(100) NOT NULL,
  `notes` varchar(100) NOT NULL,
  `category_id` int(11) NOT NULL,
  `designs_tags` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `designs` WRITE;
/*!40000 ALTER TABLE `designs` DISABLE KEYS */;

INSERT INTO `designs` (`id`, `image`, `challenge_id`, `title`, `story`, `notes`, `category_id`, `designs_tags`, `user_id`, `created`)
VALUES
	(1,'/files/uploads/pitcher.jpg',1,'test','test','test',1,0,1,NULL),
	(2,'/files/uploads/pitcher.jpg',1,'summer','summers','summer',1,0,1,NULL),
	(3,'/files/uploads/orange-set.jpg',1,'Orange Set','Story of orange set','test',1,0,3,NULL);

/*!40000 ALTER TABLE `designs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;

INSERT INTO `groups` (`id`, `name`, `created`, `modified`)
VALUES
	(1,'Admin','2012-01-15 16:16:09','2012-01-15 16:16:09'),
	(2,'Member','2012-01-15 16:16:16','2012-01-15 16:16:16');

/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `body` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;

INSERT INTO `posts` (`id`, `title`, `body`, `created`, `modified`)
VALUES
	(1,'The title','This is the post body.','2012-01-15 16:20:44',NULL),
	(2,'A title once again','And the post body follows.','2012-01-15 16:20:44',NULL),
	(3,'Title strikes back','This is really exciting! Not.','2012-01-15 16:20:44',NULL);

/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table prizes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `prizes`;

CREATE TABLE `prizes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `challenge_id` int(11) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `prizes` WRITE;
/*!40000 ALTER TABLE `prizes` DISABLE KEYS */;

INSERT INTO `prizes` (`id`, `challenge_id`, `name`, `created`, `created_by`, `modified`, `modified_by`)
VALUES
	(1,1,'$2,000 cash','2014-01-26 12:47:43','','2014-01-26 12:47:43',''),
	(2,1,'$500 Threadless gift code','2014-01-26 12:47:55','','2014-01-26 12:47:55',''),
	(3,1,'Custom skateboard','2014-01-26 12:48:04','','2014-01-26 12:48:04','');

/*!40000 ALTER TABLE `prizes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tags`;

CREATE TABLE `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table themes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `themes`;

CREATE TABLE `themes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `themes` WRITE;
/*!40000 ALTER TABLE `themes` DISABLE KEYS */;

INSERT INTO `themes` (`id`, `name`)
VALUES
	(1,'Raya Cina');

/*!40000 ALTER TABLE `themes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `username` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` tinytext COLLATE utf8_unicode_ci COMMENT 'full url to avatar image file',
  `language` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `modified_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `group_id`, `username`, `name`, `password`, `email`, `avatar`, `language`, `timezone`, `token`, `status`, `created`, `modified`, `last_login`, `created_by`, `modified_by`)
VALUES
	(1,1,NULL,'Admin','ab6ca9fb7e2731ef145b21f6a0c2bcb9efe1b5e9','admin@nucleus.com.my',NULL,NULL,NULL,'4f129978-fd3c-445e-8239-2feb2b2926db',1,'2012-01-15 16:16:40','2012-01-15 16:16:40','0000-00-00 00:00:00','',''),
	(2,2,NULL,'Member','12da3e452ea3a4ba9bb340a24e4e2c15232a7d42','member@dev.com',NULL,NULL,NULL,'4f12998d-a7cc-4b64-8d13-30292b2926db',1,'2012-01-15 16:17:01','2012-01-15 16:17:01','0000-00-00 00:00:00','',''),
	(3,1,NULL,'Ramzan','ab6ca9fb7e2731ef145b21f6a0c2bcb9efe1b5e9','ramzan.rozali@gmail.com',NULL,NULL,NULL,'336d184d485b0fdce6cb9fa732368099',1,'2014-01-21 03:07:02','2014-01-21 03:07:02','0000-00-00 00:00:00','','');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table votes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `votes`;

CREATE TABLE `votes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `design_id` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
