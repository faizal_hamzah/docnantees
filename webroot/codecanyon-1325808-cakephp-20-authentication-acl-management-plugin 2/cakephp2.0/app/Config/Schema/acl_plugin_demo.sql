/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50508
Source Host           : localhost:3306
Source Database       : acl_plugin

Target Server Type    : MYSQL
Target Server Version : 50508
File Encoding         : 65001

Date: 2012-02-07 09:33:05
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `acl_acos`
-- ----------------------------
DROP TABLE IF EXISTS `acl_acos`;
CREATE TABLE `acl_acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of acl_acos
-- ----------------------------
INSERT INTO `acl_acos` VALUES ('1', null, null, null, 'controllers', '1', '58');
INSERT INTO `acl_acos` VALUES ('2', '1', null, null, 'Posts', '2', '15');
INSERT INTO `acl_acos` VALUES ('3', '2', null, null, 'home', '3', '4');
INSERT INTO `acl_acos` VALUES ('4', '2', null, null, 'index', '5', '6');
INSERT INTO `acl_acos` VALUES ('5', '2', null, null, 'view', '7', '8');
INSERT INTO `acl_acos` VALUES ('6', '2', null, null, 'add', '9', '10');
INSERT INTO `acl_acos` VALUES ('7', '2', null, null, 'edit', '11', '12');
INSERT INTO `acl_acos` VALUES ('8', '2', null, null, 'delete', '13', '14');
INSERT INTO `acl_acos` VALUES ('9', '1', null, null, 'AclManagement', '16', '57');
INSERT INTO `acl_acos` VALUES ('10', '9', null, null, 'Groups', '17', '28');
INSERT INTO `acl_acos` VALUES ('11', '10', null, null, 'index', '18', '19');
INSERT INTO `acl_acos` VALUES ('12', '10', null, null, 'view', '20', '21');
INSERT INTO `acl_acos` VALUES ('13', '10', null, null, 'add', '22', '23');
INSERT INTO `acl_acos` VALUES ('14', '10', null, null, 'edit', '24', '25');
INSERT INTO `acl_acos` VALUES ('15', '10', null, null, 'delete', '26', '27');
INSERT INTO `acl_acos` VALUES ('16', '9', null, null, 'Permissions', '29', '38');
INSERT INTO `acl_acos` VALUES ('17', '16', null, null, 'index', '30', '31');
INSERT INTO `acl_acos` VALUES ('18', '16', null, null, 'sync', '32', '33');
INSERT INTO `acl_acos` VALUES ('19', '16', null, null, 'edit', '34', '35');
INSERT INTO `acl_acos` VALUES ('20', '16', null, null, 'toggle', '36', '37');
INSERT INTO `acl_acos` VALUES ('21', '9', null, null, 'Users', '39', '56');
INSERT INTO `acl_acos` VALUES ('22', '21', null, null, 'login', '40', '41');
INSERT INTO `acl_acos` VALUES ('23', '21', null, null, 'logout', '42', '43');
INSERT INTO `acl_acos` VALUES ('24', '21', null, null, 'index', '44', '45');
INSERT INTO `acl_acos` VALUES ('25', '21', null, null, 'view', '46', '47');
INSERT INTO `acl_acos` VALUES ('26', '21', null, null, 'add', '48', '49');
INSERT INTO `acl_acos` VALUES ('27', '21', null, null, 'edit', '50', '51');
INSERT INTO `acl_acos` VALUES ('28', '21', null, null, 'delete', '52', '53');
INSERT INTO `acl_acos` VALUES ('29', '21', null, null, 'toggle', '54', '55');

-- ----------------------------
-- Table structure for `acl_aros`
-- ----------------------------
DROP TABLE IF EXISTS `acl_aros`;
CREATE TABLE `acl_aros` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of acl_aros
-- ----------------------------
INSERT INTO `acl_aros` VALUES ('1', null, 'Group', '1', null, '1', '4');
INSERT INTO `acl_aros` VALUES ('2', null, 'Group', '2', null, '5', '8');
INSERT INTO `acl_aros` VALUES ('3', '1', 'User', '1', null, '2', '3');
INSERT INTO `acl_aros` VALUES ('4', '2', 'User', '2', null, '6', '7');

-- ----------------------------
-- Table structure for `acl_aros_acos`
-- ----------------------------
DROP TABLE IF EXISTS `acl_aros_acos`;
CREATE TABLE `acl_aros_acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) NOT NULL,
  `aco_id` int(10) NOT NULL,
  `_create` varchar(2) NOT NULL DEFAULT '0',
  `_read` varchar(2) NOT NULL DEFAULT '0',
  `_update` varchar(2) NOT NULL DEFAULT '0',
  `_delete` varchar(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ARO_ACO_KEY` (`aro_id`,`aco_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of acl_aros_acos
-- ----------------------------
INSERT INTO `acl_aros_acos` VALUES ('1', '1', '1', '1', '1', '1', '1');
INSERT INTO `acl_aros_acos` VALUES ('2', '2', '7', '1', '1', '1', '1');
INSERT INTO `acl_aros_acos` VALUES ('3', '2', '6', '1', '1', '1', '1');

-- ----------------------------
-- Table structure for `acl_groups`
-- ----------------------------
DROP TABLE IF EXISTS `acl_groups`;
CREATE TABLE `acl_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of acl_groups
-- ----------------------------
INSERT INTO `acl_groups` VALUES ('1', 'Administrator', '2012-02-07 03:17:18', '2012-02-07 03:17:18');
INSERT INTO `acl_groups` VALUES ('2', 'Member', '2012-02-07 03:17:35', '2012-02-07 03:17:35');

-- ----------------------------
-- Table structure for `acl_posts`
-- ----------------------------
DROP TABLE IF EXISTS `acl_posts`;
CREATE TABLE `acl_posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `body` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of acl_posts
-- ----------------------------
INSERT INTO `acl_posts` VALUES ('1', 'asd', 'asd', '2012-01-05 11:08:56', '2012-01-31 03:28:22');
INSERT INTO `acl_posts` VALUES ('14', 'ty', 'drty', '2012-02-01 22:42:23', '2012-02-01 22:42:23');
INSERT INTO `acl_posts` VALUES ('15', 'bhi', 'haahahahha', '2012-02-02 04:23:32', '2012-02-02 04:23:32');
INSERT INTO `acl_posts` VALUES ('16', 'Test Post', 'About test post', '2012-02-04 22:18:40', '2012-02-06 08:23:20');
INSERT INTO `acl_posts` VALUES ('17', 'sdfwe', 'fdewdewdew', '2012-02-06 11:15:44', '2012-02-06 11:15:44');
INSERT INTO `acl_posts` VALUES ('11', 'greg', 'greg', '2012-01-21 14:34:51', '2012-01-21 14:34:51');
INSERT INTO `acl_posts` VALUES ('12', 'Hello dfg dfg df', 'Im just testing', '2012-01-29 10:21:02', '2012-01-29 23:10:19');
INSERT INTO `acl_posts` VALUES ('13', '12121212', 'wer edrgdfgd fgfh fgdf', '2012-01-29 23:11:16', '2012-02-01 22:32:56');
INSERT INTO `acl_posts` VALUES ('3', '1914 translation by H. Rackham', 'On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.', '2012-01-14 04:50:32', '2012-02-01 22:42:58');
INSERT INTO `acl_posts` VALUES ('4', 'Finibus Bonorum et Malorum, written by Cicero in 4', 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.', '2012-01-14 04:53:17', '2012-01-17 10:34:28');

-- ----------------------------
-- Table structure for `acl_users`
-- ----------------------------
DROP TABLE IF EXISTS `acl_users`;
CREATE TABLE `acl_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `username` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` tinytext COLLATE utf8_unicode_ci COMMENT 'full url to avatar image file',
  `language` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of acl_users
-- ----------------------------
INSERT INTO `acl_users` VALUES ('1', '1', null, 'Admin', '12da3e452ea3a4ba9bb340a24e4e2c15232a7d42', 'admin@dev.com', null, null, null, '4f3089ea-aaac-4b67-8bca-0fa02b2926db', '1', '2012-02-07 03:18:18', '2012-02-07 03:18:18', '0000-00-00 00:00:00');
INSERT INTO `acl_users` VALUES ('2', '2', null, 'Member', '12da3e452ea3a4ba9bb340a24e4e2c15232a7d42', 'member@dev.com', null, null, null, '4f308a0b-1674-4fc6-8027-0fa02b2926db', '1', '2012-02-07 03:18:51', '2012-02-07 03:18:51', '0000-00-00 00:00:00');
