<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    public $helpers = array(
    'Facebook.Facebook' => array(
        'appId' => '264524597040666',
        'appSecret' => 'e1f9aac54f38777b7f438cb5ea106938'
    )
    );

    public $theme = "Flatlab";

	public $components = array(
         'Facebook.Facebook' => array(
        'appId' => '264524597040666',
        'appSecret' => 'e1f9aac54f38777b7f438cb5ea106938'
        ),
        'Acl',
        'Auth' => array(
            'authenticate' => array(
                'Form' => array(
                    'fields' => array('username' => 'email'),
                    'scope'  => array('User.status' => 1)
                )
            ),
            'authorize' => array(
                'Actions' => array('actionPath' => 'controllers')
            )
        ),
        'Session'
    );

    public function beforeFilter() {
     parent::beforeFilter();
 
     $this->Auth->allow("*");//must comment after generate action
 
     //Configure AuthComponent
     $this->Auth->loginAction = '/users/login';
     $this->Auth->logoutRedirect = '/users/login';
     $this->Auth->loginRedirect = array('plugin'=>false, 'controller' => 'designs', 'action' => 'index');
 	}
}
