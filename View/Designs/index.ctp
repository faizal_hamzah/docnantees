<!--sidebar menu-->
 <?php echo $this->element('menu/sidebar'); ?>
<!--design section-->
<div class="col-xs-12 col-sm-6 col-lg-8">
    <div class="row container">
            <div class="gallery-container">
                <h2><?php echo __('Designs'); ?></h2>
                <!-- <ul id="filters" class="list-unstyled">
                    <li><a href="#" data-filter="*"> All</a></li>
                    <li><a href="#" data-filter=".design">Design</a></li>
                    <li><a href="#" data-filter=".development">Development</a></li>
                    <li><a href="#" data-filter=".frontend, .dashboard">Frontend</a></li>
                    <li><a href="#" data-filter=".dashboard">Dashboard</a></li>
                    <li><a href="#" data-filter=".flat:not(.development)">Flat</a></li>
                </ul>
                 -->
                 <div id="gallery" class="col-4">
                    <?php foreach ($designs as $design): ?>
                    <input type="hidden" value="<?php h($design['Design']['id']);?>" />
                    <div class="element design development item view view-tenth" data-zlname="reverse-effect">
                       <?php echo $this->Html->image($design['Design']['image'] , array('fullBase' => true ,'class' => 'course-img'));?>
                        <div class="mask">
                            <h2>Freshness </h2>
                            <a data-zl-popup="link" href="javascript:;">
                                <i class="fa fa-link"></i>
                            </a>
                            <a data-zl-popup="link2" class="fancybox" rel="group" href="img/works/img1.jpg">
                                <i class="fa fa-search"></i>
                            </a>
                        </div>
                    </div>
                <?php endforeach;?>
                </div>
            </div>
        </div>
        <div class="row container">
        <?php
        echo $this->Paginator->counter(array(
        'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
        ));
        ?>
        </div>
        <div class="row container">
        <ul class="pagination">
                          <li><?php echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled')); ?> </li>
                          <li><?php echo $this->Paginator->numbers(array('separator' => '')); ?> </li>
                          <li><?php echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));?></li>
        </ul>
        </div>
    </div>
</div>
    
