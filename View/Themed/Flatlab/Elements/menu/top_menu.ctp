<?php $current_page = $this->params['action']; ?>
<div class="navbar-collapse collapse ">
    <ul class="nav navbar-nav">
       	<li class="active"><?php echo $this->Html->link(__('Home'), array('controller'=>'challenges','action' => 'index')); ?></li>
       	<li><?php echo $this->Html->link(__('Challenges'), array('controller'=>'challenges','action' => 'index')); ?></li>
        <li><?php echo $this->Html->link(__('Designs'), array('controller'=>'designs','action' => 'index')); ?></li>
        <li class="dropdown">
        	<a href="#" class="dropdown-toggle" data-toggle="dropdown">Admin<b class="caret"></b></a>
        	<ul class="dropdown-menu">
        	    <li><?php echo $this->Html->link(__('Users'), array('controller'=>'users','action' => 'index')); ?></li>
				<li><?php echo $this->Html->link(__('Banners'), array('controller'=>'banners','action' => 'index')); ?></li>
        		<li><?php echo $this->Html->link(__('Categories'), array('controller'=>'categories','action' => 'index')); ?></li>
        		<li><?php echo $this->Html->link(__('Vote'), array('controller'=>'votes','action' => 'index')); ?></li>        
        		<li><?php echo $this->Html->link(__('Tags'), array('controller'=>'tags','action' => 'index')); ?></li>
        	</ul>
        </li>					
    </ul>
</div>
            
<!-- <div class="navbar navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container">
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			<a class="brand" target="_blank" href="http://twitter.github.com/bootstrap/">Bootstrap</a>
			<div class="nav-collapse">
				<ul class="nav">
					<li <?php if($current_page=="index"){echo'class="active"';} ?>>
						<?php echo $this->Html->link('Scaffolding', array('controller' => 'app', 'action' => 'index')); ?>
					</li>
					<li <?php if($current_page=="base_css"){echo'class="active"';} ?>>
						<?php echo $this->Html->link('Base CSS', array('controller' => 'app', 'action' => 'base_css')); ?>
					</li>
					<li <?php if($current_page=="components"){echo'class="active"';} ?>>
						<?php echo $this->Html->link('Components', array('controller' => 'app', 'action' => 'components')); ?>
					</li>
					<li <?php if($current_page=="javascript"){echo'class="active"';} ?>>
						<?php echo $this->Html->link('Javascript plugins', array('controller' => 'app', 'action' => 'javascript')); ?>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div> -->