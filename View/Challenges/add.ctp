<!-- <div class="challenges form"> -->
<?php echo $this->Form->create('Challenge', array('type'=>'file','class'=>'form-group')); ?>
	<fieldset>
		<legend><?php echo __('Add Challenge'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('descriptions');
		echo $this->Form->input('image', array('type'=>'file'));
		echo $this->Form->input('header');
		echo $this->Form->input('shortcode');
	?>
	</fieldset>
<?php echo $this->Form->submit(__('Submit',true), array('class'=>'btn btn-primary')); echo $this->Form->end();?>    
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Challenges'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Prizes'), array('controller' => 'prizes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prize'), array('controller' => 'prizes', 'action' => 'add')); ?> </li>
	</ul>
</div>
