<div class="challenges form">
<?php echo $this->Form->create('Challenge'); ?>
	<fieldset>
		<legend><?php echo __('Edit Challenge'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('descriptions');
		echo $this->Form->input('header');
		echo $this->Form->input('startDate');
		echo $this->Form->input('endDate');
		echo $this->Form->input('shortcode');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Challenge.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Challenge.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Challenges'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Prizes'), array('controller' => 'prizes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prize'), array('controller' => 'prizes', 'action' => 'add')); ?> </li>
	</ul>
</div>
