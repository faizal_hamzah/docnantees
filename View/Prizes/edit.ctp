<div class="prizes form">
<?php echo $this->Form->create('Prize'); ?>
	<fieldset>
		<legend><?php echo __('Edit Prize'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('challenge_id');
		echo $this->Form->input('name');
		echo $this->Form->input('created_by');
		echo $this->Form->input('modified_by');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Prize.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Prize.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Prizes'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Challenges'), array('controller' => 'challenges', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Challenge'), array('controller' => 'challenges', 'action' => 'add')); ?> </li>
	</ul>
</div>
