<div class="prizes view">
<h2><?php echo __('Prize'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($prize['Prize']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Challenge'); ?></dt>
		<dd>
			<?php echo $this->Html->link($prize['Challenge']['name'], array('controller' => 'challenges', 'action' => 'view', $prize['Challenge']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($prize['Prize']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($prize['Prize']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created By'); ?></dt>
		<dd>
			<?php echo h($prize['Prize']['created_by']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($prize['Prize']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified By'); ?></dt>
		<dd>
			<?php echo h($prize['Prize']['modified_by']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Prize'), array('action' => 'edit', $prize['Prize']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Prize'), array('action' => 'delete', $prize['Prize']['id']), null, __('Are you sure you want to delete # %s?', $prize['Prize']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Prizes'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Prize'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Challenges'), array('controller' => 'challenges', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Challenge'), array('controller' => 'challenges', 'action' => 'add')); ?> </li>
	</ul>
</div>
