<?php
App::uses('AppModel', 'Model');
/**
 * Challenge Model
 *
 * @property Prize $Prize
 */
class Challenge extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Prize' => array(
			'className' => 'Prize',
			'foreignKey' => 'challenge_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Design' => array(
			'className' => 'Design',
			'foreignKey' => 'challenge_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	public $actsAs = array(
	'Containable',
	'Uploader.Attachment' => array(
		// Do not copy all these settings, it's merely an example
		'image' => array(
			'maxWidth' => 1200,
			'maxHeight' => 1200,
			'extension' => array('gif', 'jpg', 'png', 'jpeg'),
			'nameCallback' => '',
			'append' => '',
			'prepend' => '',
			//'tempDir' => TMP,
			'uploadDir' => '',
			'transportDir' => '',
			'finalPath' => '',
			'dbColumn' => '',
			'metaColumns' => array(),
			'defaultPath' => '',
			'overwrite' => true,
			'transforms' => array(),
			'stopSave' => true,
			'allowEmpty' => true,
			'transformers' => array(),
			'transport' => array(),
			'transporters' => array(),
			'curl' => array()
		)
	)
	);

}
